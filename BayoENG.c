#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

#if defined(_WIN32) || defined(_WIN64)
  
  #define WINDOWS = 1;
  #include <shlwapi.h>
  #include <direct.h>
  
  void setupOutFolderWin(){
     _mkdir("./out");
  }

#endif

#define VERBOSE 0

bool compress;

typedef struct line{
  bool control;
  size_t code;
  size_t lenght;
  unsigned char content[512];
} line;

typedef struct msg{
    size_t pos;
    size_t order;
    size_t lenght;
    size_t line_count;
    unsigned char* content;
    line lines[256];
}  msg;

unsigned long lastRead = 0;

void newControlLine(line* l,char* buf){
  //printf("Making new Control Line!(%02hhx%02hhx %02hhx%02hhx)\n",buf[0],buf[1],buf[2],buf[3]);
  memcpy(l->content, buf, 4);
  l->lenght=4;
  l->control=true;
}

void storeLine(msg* message, line* line){
    printf("Storing line %lu of message %lu of lenght %lu...",message->line_count,message->order,line->lenght );
    //printf("reallocating for more than %lu lines\n", message->line_count );
    //message->lines=realloc(message->lines, message->line_count+1);
    //printf("Succesfully reallocated lines for %lu lines\n",message->line_count+1 );
    message->lines[message->line_count]=*line;
    message->line_count++;
      printf("Stored! (%s)\n", line->control?"Control":"Text");
}

void partitionMessage(msg* message){
  printf("Partitioning message %lu into lines\n",message->order );
  line* currentLine = calloc(sizeof(line),1);
  for (size_t i = 0; i < message->lenght;) {
    if(message->content[i]==0x40 || message->content[i]==0x01 || message->content[i]==0x20){
      printf("Detected Control Line\n" );
      if(currentLine->lenght>0)  storeLine(message, currentLine);
      newControlLine(currentLine,message->content+i);
      i+=4;
      storeLine(message, currentLine);
      currentLine=calloc(sizeof(line),1);
    }else{
      currentLine->control=false;
      currentLine->content[(currentLine->lenght)++]=message->content[i++];
      currentLine->content[(currentLine->lenght)++]=message->content[i++];
    }
    }
    storeLine(message, currentLine);
    printf("Message %lu partitioned",message->order);
}

int loadMessage(FILE* f ,msg* message){
    //printf("loadmessage Loading %04lu\n",message->order);
    message->content = malloc(message->lenght);
    fseek(f,message->pos,SEEK_SET);
    fread(message->content,1,message->lenght,f);
    //printf("Loading #%lu: %s\n",message->order, strerror(errno) );
    return 1;
}


uint32_t getNextWord(FILE* f){
    uint8_t buf[4] ={0,0,0,0};
    long unsigned start =ftell(f);
    long unsigned cur;
    size_t read;
    if(VERBOSE) printf("Reading from %04lu (%04lu)\n",start, lastRead);
    for (int i=0; i<4;i++){
        fseek(f,lastRead,SEEK_SET);
        read= fread(buf+i,1,1,f);
        cur = ftell(f);
        if(lastRead+1!=cur){
            printf("ERROR(%lu): SHOULD BE AT %lu BUT AM IN %lu \n\tOFF BY %lu, READ %lu BYTES (SHOULD BE 1)\n\t",lastRead,lastRead+1,cur, cur-(lastRead+1),read);
            for(int j=0;j<i;j++){
                printf("%02x ",buf[j]);
            }
            printf("HAS BEEN READ THUS FAR\n");
        }
        lastRead++;
    }

    uint32_t ret = *((uint32_t*) buf);
    if (VERBOSE) printf("Read %04x\n",ret);
    return ret;
}

void forcefulPrint(msg m){
    printf("Printing msg:%lu, of lenght %lu, character-by-character:\n",m.order,m.lenght);
    int i;
    for(i = 0; i<m.lenght;i+=2){
        if(((unsigned char)(m.content[i])) == 0x40) printf("\n");
        printf("%02hhx",(unsigned char)(m.content[i]));
        printf("%02hhx ",(unsigned char)(m.content[i+1]));
    }
    printf("\n");
}




void linePrint(msg message, FILE* stream){
  for (size_t i = 0; i < message.line_count; i++) {
    line l = message.lines[i];
    fprintf(stream,"%c #%04lu >{",l.control?'C':'T',i);
    printf("\tPrinting %s line %lu/%lu.\n\tLenght: %lu\n",l.control?"Control":"Text",i,message.line_count,l.lenght);
    if(l.control){
      fprintf(stream,"%02hhx%02hhx %02hhx%02hhx}<\n",l.content[0],l.content[1],l.content[2],l.content[3]);
    }else{
      for (size_t j = 0; j < l.lenght; j++) {
        fputc(l.content[j],stream);
      }
      fprintf(stream,"}<\n");
    }
  }
}


int main(int argc, char const *argv[]){
    FILE* sq;
    size_t fileSize;
    uint32_t cur,next,i,first,count;
    msg currentMsg;
    msg* msgArray;
    char txt[20];


    if(argc<2){
        printf("Please insert an .sq file to read\n");
        return 1;
    }

    if(argv[1][strlen(argv[1])-1]=='t'){
      compress = true;
      FILE* sqtxt;
      memcpy(txt, argv[1], strlen(argv[1]));
      sqtxt=fopen(argv[1], "rb");
      if(sqtxt==0) {
        printf("Error opening %s: %s\n",txt,strerror(errno) );
        return 1;
      }
      txt[strlen(argv[1])-4]='\0';
      printf("Repacking script from %s into %s\n",argv[1],txt );
      sq=fopen(txt,"wb");
      if(sq==0) {
        printf("Error opening %s: %s\n",argv[1],strerror(errno) );
        return 1;
      }
      bool reading = true;
      char lineBuffer[4096];
      i=0;
      fileSize=0;
      size_t lineI;
      size_t readLenght;
      size_t ignore;
      bool eol=false;
      while (reading) {
        readLenght=0;
        for (size_t z = 0; !eol && readLenght<1024; z++) {
            lineBuffer[z]=fgetc(sqtxt);
            eol=((lineBuffer[z-1]=='}') && (lineBuffer[z]=='<'));
            printf("%c",lineBuffer[z]);
            readLenght++;
          }
          if(readLenght==1024){
            printf("Error reaching eol:%s\n",strerror(errno));
            return 1;
          }
          fgetc(sqtxt);
          eol=false;
        printf("\nScanned line (%c)\n",lineBuffer[0]);
        switch (lineBuffer[0]) {
          case 'N': // first line, number of messages in the file
              sscanf(lineBuffer, "Number of Messages: %u\n", &count);
              printf("Will pack %u messages\n",count);
              fileSize+=((count+1)*4);
              msgArray=calloc(sizeof(msg),count);
            break;
          case 'M': //Message number header
              sscanf(lineBuffer, "Message #%u\n",&i);
              printf("Reading Message %u\n",i);
              lineI=0;
              msgArray[i] = (msg){fileSize,i,0, 0, NULL};
            break;
          case 'C': //Control type Line
              msgArray[i].lines[lineI].control=true;
              sscanf(lineBuffer,"C #%lu >{%02hhx%02hhx %02hhx%02hhx}<\n",&ignore,
                msgArray[i].lines[lineI].content,
                msgArray[i].lines[lineI].content+1,
                msgArray[i].lines[lineI].content+2,
                msgArray[i].lines[lineI].content+3);
                msgArray[i].lines[lineI].lenght=4;
                msgArray[i].lenght+=msgArray[i].lines[lineI].lenght;
                msgArray[i].line_count++;
                printf("Read Control Type Line #%lu of lenght %lu\n",lineI,msgArray[i].lines[lineI].lenght);
                lineI++;
            break;
          case 'T': //Text Type Line
                msgArray[i].lines[lineI].control=false;
                //sscanf(lineBuffer,"T #%lu  \n",&lineI);
                memcpy(msgArray[i].lines[lineI].content, lineBuffer+10, readLenght-12);
                msgArray[i].lines[lineI].lenght=readLenght-12;
                msgArray[i].lenght+=msgArray[i].lines[lineI].lenght;
                msgArray[i].line_count++;
                printf("Read Text Type Line #%lu of lenght %lu\n",lineI,msgArray[i].lines[lineI].lenght);
                lineI++;
            break;
          case 'E'://End Of Message
            printf("End of Message %d, ran from %lu to %lu (%lu)\n",i,fileSize,fileSize+msgArray[i].lenght,msgArray[i].lenght);
            /*if(msgArray[i].lenght==0){
              printf("Adjusting 0 lenght messages to have 1 byte of lenght\n", );
            }*/
            fileSize+=msgArray[i].lenght;
            reading=(i!=count-1);
            break;
        }
      }

    }else{
      compress = false;
      sq=fopen(argv[1],"rb");
      fseek(sq,0,SEEK_END);
      fileSize=ftell(sq);
      fseek(sq,0,SEEK_SET);

      first = getNextWord(sq);
      count = (first/4)-1;

      //uint32_t* buffer = calloc(first,1);
      printf("Unpacking %s: First message found at %x (%u)there's %u messages\n",argv[1],first,first, count);

      msgArray = malloc(sizeof(msg)*count);
      next = getNextWord(sq);

      for (i = 0; i<count;i++){ //Message structure generation loop
          cur=next;
          next= getNextWord(sq);
          msgArray[i] = (msg){cur,i,next-cur, 0, NULL};
      }
      msgArray[--i].lenght=fileSize-cur;
      printf("Generated %u messages\n",i );

      for (i = 0; i<count;i++){ //message loading loop
          loadMessage(sq,msgArray+i);
          printf("Message %04u at %04lu (%x) has length %04lu\n",i ,msgArray[i].pos,msgArray[i].pos, msgArray[i].lenght);
      }
    }
    /*
    if(argc>2){
      if(argv[2][1]=='e'){

        char out[20]="";
        char ext[6]=".msg";
        char oth[6]=".sjis";

        int ret;
        memcpy(out, argv[3],strlen(argv[3]));

        size_t msgID = atoi(argv[3]);
        printf("Outputting msg #%lu contents to rawlog\n",msgID );
        msg m = msgArray[msgID];

        memcpy(out+strlen(argv[3]), ext, 5);
        FILE* json=fopen("rawlog","wb");
        for(size_t y =0;y<m.lenght;y++){
          ret+=fputc(m.content[y], json);
        }
        //ret = fwrite(&(m.content),sizeof(char),m.lenght,json);
        printf("for %lu wrote %d (%lu)\n",m.order,ret,m.lenght);

        memcpy(out+strlen(argv[3]), oth, 5);
        forcefulPrint(m);
        FILE* structlog=fopen("structlog","wb");
        partitionMessage(&m);
        linePrint(m, structlog);
      }else
      if(argv[2][1]=='s'){
        size_t msgID = atoi(argv[3]);
        msg m = msgArray[msgID];
        forcefulPrint(m);
        printf("Setting up lines\n");
        partitionMessage(&m);
        linePrint(m, stdout);
      }
    }else*/
    if (compress){
      uint8_t outBuf[4] ={0,0,0,0};
      printf("Will now Store into %s\n",txt );
      /*outBuf[0]= msgArray[0].pos;
      outBuf[1]= msgArray[0].pos<<8;
      outBuf[2]= msgArray[0].pos<<16;
      outBuf[3]= msgArray[0].pos<<24;
      */
      *((uint32_t*)(outBuf))=msgArray[0].pos;
      printf("%02hhx%02hhx %02hhx%02hhx\n", outBuf[0],outBuf[1],outBuf[2],outBuf[3]);
      fputc(outBuf[0],sq);
      fputc(outBuf[1],sq);
      fputc(outBuf[2],sq);
      fputc(outBuf[3],sq);
      //      fprintf(sq, "%lu",msgArray[0].pos); //print bytes not string!!! TODO TODO TODO
      for (i = 0; i < count; i++){
        *((uint32_t*)(outBuf))=msgArray[i].pos;
        printf("%02hhx%02hhx %02hhx%02hhx\n", outBuf[0],outBuf[1],outBuf[2],outBuf[3]);
        fputc(outBuf[0],sq);
        fputc(outBuf[1],sq);
        fputc(outBuf[2],sq);
        fputc(outBuf[3],sq);
      }
      for (i = 0; i < count; i++){
        printf("Storing Message #%u\n",i);
        for (size_t j = 0; j < msgArray[i].line_count; j++) {
          printf("Line #%lu of lenght %lu;",j,msgArray[i].lines[j].lenght);
          if(msgArray[i].lines[j].control) {
          printf("Control line: %02hhx%02hhx %02hhx%02hhx", msgArray[i].lines[j].content[0],msgArray[i].lines[j].content[1],msgArray[i].lines[j].content[2],msgArray[i].lines[j].content[3]);
        }else{
          printf("Text Line:");
          for(size_t z =0;z<msgArray[i].lines[j].lenght;z++){ printf("%c",msgArray[i].lines[j].content[z]);}
        }
        printf(" Printed: ");
        for(size_t z =0;z<msgArray[i].lines[j].lenght;z++){ fputc(msgArray[i].lines[j].content[z],sq);printf("%02hhx ",msgArray[i].lines[j].content[z]);}
        printf("\n" );
        }
      }
      fseek(sq,-1,SEEK_END);
      _chsize((_fileno( sq ) ),ftell(sq));
      printf("Recompression of script %s Succesfully done\n",argv[1] );
    }else{
      /*
      #ifdef WINDOWS
        setupOutFolderWin();
      #endif
      char outFile[40] = "./out/";
      memcpy(outFile+6,argv[1], strlen(argv[1]));
      memcpy(outFile+6+strlen(argv[1]),".txt", 5);
      */
     char outFile[40] = ""; 
      memcpy(outFile,argv[1], strlen(argv[1]));
      memcpy(outFile+strlen(argv[1]),".txt", 5);
      FILE* fullFileLog= fopen(outFile, "wb");
      printf("Outputting file into %s: %s\n",outFile,strerror(errno));
      fprintf(fullFileLog, "Number of Messages: %u}<\n",count );
      for (i = 0; i < count; i++){
        printf("Message #%u: It's text is:\n",i );
        currentMsg = msgArray[i];
        partitionMessage(&currentMsg);
        fprintf(fullFileLog, "Message #%u}<\n",i );
        linePrint(currentMsg,fullFileLog);
        fprintf(fullFileLog, "End Message}<\n");
        //forcefulPrint(currentMsg);
        //gracefulPrint(currentMsg);
      }
      printf("Extraction of script %s Succesfully done\n",argv[1] );
    }
    return 0;
}
